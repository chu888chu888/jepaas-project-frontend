/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-16 15:40:44
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-16 15:41:30
 */
import Vue from 'vue';
import App from './index.vue';


Vue.config.productionTip = false;
Vue.config.devtools = true;

new Vue({
  render: h => h(App),
}).$mount('#app');
