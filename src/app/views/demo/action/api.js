/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-16 16:23:05
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-16 16:27:26
 */
// 所有的接口

// 服务器地址
import { HOST_BASIC } from '../../../constants/config';

// 冒泡列表
export const POST_FIND_BUBBLE_MSG = `${HOST_BASIC}/je/portal/homePortal/findBubbleMsg`;

