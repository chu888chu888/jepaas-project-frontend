/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-16 16:23:56
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-16 16:47:23
 */

// 对接口返回来的参数进行处理
import fetch from '@/util/fetch';  // 引入请求
import { POST_FIND_BUBBLE_MSG } from './api';

/*
 * 获取冒泡的列表数据
 * @param {position}
 * @param {type}
 * @param {start}
 * @param {limit} 每页多少条
 * @return: Object res.obj.rows
 */
export function getBubbleList(param) {
  return fetch(POST_FIND_BUBBLE_MSG, null, {
    type: 'POST',
    data: param,
  })
    .then(data => {
      if (data.success) {
        return data.obj.rows || [];
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}
