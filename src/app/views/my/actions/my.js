import {
  POST_DO_UPDATE,
  POST_DO_SAVE,
} from './api';

/**
 *
 *  压缩图片
 *
 * @export
 * @param {path} string 图片位置
 * @returns {Promise }
 */
export function zipImg(path) {
  return new Promise((resolve, reject) => {
    plus.zip.compressImage({
      src: path,
      dst: `_doc/temp/${+new Date()}.jpg`,
      quality: 70,
    },
    ({ target }) => {
      resolve(target);
    },
    (e) => {
      reject(e);
    });
  });
}
/**
 *
 *  上传图片
 *
 * @export
 * @param {*} { path, hideWaiting }
 *  1、压缩图片，{
      PHOTO: item,
      tableCode: 'JE_CORE_ENDUSER',
      USERID: JE.getCurrentUser().id,
      DEPTID: JE.getCurrentUser().deptId,
      funcCode: 'JE_CORE_ENDUSER',
      uploadableFields: 'PHOTO',
      jeFileType: 'PROJECT',
    }
 *
 * @returns {Promise ALL}
 */
export function uploaderImg(params) {
  return JE.fetch(POST_DO_UPDATE, null, {
    type: 'POST',
    data: params,
  })
    .then(data => data)
    .catch();
}
export function saveUserImg(params) {
  return JE.fetch(POST_DO_SAVE, null, {
    type: 'POST',
    data: params,
  })
    .then(data => data)
    .catch();
}
