// 服务器地址
import { HOST_BASIC } from '../../../constants/config';

// 上传头像
export const POST_DO_UPDATE = `${HOST_BASIC}/je/document/fileBase64`;
export const POST_DO_SAVE = `${HOST_BASIC}/je/rbac/user/doUpdateInfo`;
