import {
  POST_LOGIN,
  POST_SEND_VALIDATECODE,
  POST_UPDATE_PWD,
  GET_SEARCH_COMPANY,
  POST_JOIN_COMPANY,
  GET_INDUSTRY,
  POST_HAS_USER,
  POST_CHECK_VALIDATECODE,
  POST_CHECK_USER,
} from './api';

// 简单加密参数
const compileStr = (params) => {
  const strs = { j_dept: 'd', j_username: 'u', j_password: 'p' };
  for (const key in strs) {
    if (params.hasOwnProperty(key)) {
      params[strs[key]] = window.btoa(window.btoa(encodeURIComponent(params[key])));
      delete params[key];
    }
  }
};

/**
 * 登录
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchLogin(param) {
  const { clientid, token } = JE.getPushInfo();
  const data = Object.assign(param, {
    apkId: JE.getApkID(),
    apkName: JE.getLSItem('apkName'),
    apkKey: JE.getLSItem('apkCode'),
    cid: clientid,
    token,
    type: plus.os.name,
  });
  compileStr(data);
  return JE.fetch(POST_LOGIN, {}, {
    type: 'post',
    data,
  })
    .then(res => res)
    .catch();
}
/**
 * 检查用户
 * @param param
 * @returns {Promise<T | never>}
 */
export function checkingUser(param) {
  return JE.fetch(POST_CHECK_USER, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 *发送短信验证码
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchSendValidateCode(param) {
  return JE.fetch(POST_SEND_VALIDATECODE, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}


/**
 *修改密码
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchUpdatePwd(param) {
  compileStr(param);
  return JE.fetch(POST_UPDATE_PWD, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}


/**
 *查询用户是否存在
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchHasUser(param) {
  return JE.fetch(POST_HAS_USER, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}


/**
 *查询企业
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchSearchUserCompany(param) {
  return JE.fetch(GET_SEARCH_COMPANY, param, {})
    .then(data => data)
    .catch();
}


/**
 *查询行业
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchGetIndustry(param) {
  return JE.fetch(GET_INDUSTRY, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 *添加公司
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchJoinCompany(param) {
  return JE.fetch(POST_JOIN_COMPANY, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/**
 *核对验证码
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchCheckValidate(param) {
  return JE.fetch(POST_CHECK_VALIDATECODE, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
