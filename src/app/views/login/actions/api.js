// 服务器地址

// 登录接口
export const POST_LOGIN = '/rbac/login/login';
// 发送验证码
export const POST_SEND_VALIDATECODE = '/rbac/login/sendRandom';
// 修改密码
export const POST_UPDATE_PWD = '/rbac/login/modifyPw';
// 查询企业
export const GET_SEARCH_COMPANY = '/rbac/login/searchUserCompany';
// 添加企业
export const POST_JOIN_COMPANY = '/je/app/AppUser/joinCompany';
// 查询行业
export const GET_INDUSTRY = '/je/saas/saasYh/searchUsertrade';
// 用户是否注册
export const POST_HAS_USER = '/rbac/login/validateRegister';

// 检查用户
export const POST_CHECK_USER = '/rbac/login/checkUser';

// 校验验证码
export const POST_CHECK_VALIDATECODE = '/rbac/login/validateRandom';
// 用户实体
export const POST_USER_CODE = '/rbac/login/getIdentityByUserCode';

// app
// 发送状态机
export const POST_APP_CREATE_STATE = '/je/saas/saasYh/createState';