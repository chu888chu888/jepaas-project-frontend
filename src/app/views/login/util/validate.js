export const validate = {
  // 是否为空
  isNull(str) {
    if (str) return true;
    return false;
  },
  // 验证手机号
  isPhone(phone) {
    const regPhone = /^(((13[0-9]{1})|(15[0-9]{1})|(16[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
    if (regPhone.test(phone)) return true;
    return false;
  },
  // 验证密码
  regPwd(pwd) {
    const regPwd = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
    if (regPwd.test(pwd)) return true;
    return false;
  },
  // 验证邮箱
  regEmail(email) {
    const regEmail = /^([0-9A-Za-z\-_\.]+)@([0-9a-z]+\.[a-z]{2,3}(\.[a-z]{2})?)$/g;
    if (regEmail.test(email)) return true;
    return false;
  },
  // 是否为数字
  isNum(num) {
    const regNum = /^[0-9]+$/;
    if (regNum.test(num)) return true;
    return false;
  },
};


export function toastValidate(msg, flag, optionType = '请输入') {
  if (!flag) {
    mui.toast(`${optionType}${msg}`, { duration: 'long', type: 'div' });
  }
  return flag;
}
