import {
  HOST
} from '../config'

// 登录接口
export const POST_LOGIN = `${HOST}/je/login/login`
// 发送验证码
export const POST_SEND_VALIDATECODE = `${HOST}/je/app/login/sendRandom`
// 修改密码
export const POST_UPDATE_PWD = `${HOST}/je/app/login/modifyPw`
// 注册
export const POST_REGISTER = `${HOST}/je/appRegister/registerAccount`
// 查询企业
export const GET_SEARCH_COMPANY = `${HOST}/je/app/AppUser/searchUserCompany`
// 添加企业
export const POST_JOIN_COMPANY = `${HOST}/je/app/AppUser/joinCompany`
// 查询行业
export const GET_INDUSTRY = `${HOST}/je/app/AppUser/searchUsertrade`
// 用户是否注册
export const POST_HAS_USER = `${HOST}/je/app/login/validateRegister`
// 校验验证码
export const POST_CHECK_VALIDATECODE = `${HOST}/je/app/login/validateRandom`
// 注册绑定企业
export const POST_REGISTER_ACOOUNT = `${HOST}/je/app/login/register`
// 用户实体
export const POST_USER_CODE = `${HOST}/je/app/login/getIdentityByUserCode`
// 发送状态机
export const POST_APP_CREATE_STATE = `${HOST}/je/app/login/createState`
// 第三方注册用户
export const POST_APP_REGISTER_USER = `${HOST}/je/app/login/registerDsf`
// 钉钉信息
export const POST_APP_DINGTALK_INFO = `${HOST}/je/app/login/getDingTalkInfo`
// 设置二维码
export const POST_SET_QRCODE = `${HOST}/je/app/login/setQRcode`
// 二维码轮询
export const POST_QR_LOGIN = `${HOST}/je/app/login/getQRcodeLoginInfo`
// 钉钉获取token
export const GET_DINGTALK_TOKEN = `${HOST}/je/app/TransferController/transferDingTalk`
// 钉钉获取token
export const GET_DINGTALK_PERSISTENT_CODE = 'https://oapi.dingtalk.com/sns/get_persistent_code'
// 钉钉获取token
export const GET_DINGTALK_SNS_TOKEN = 'https://oapi.dingtalk.com/sns/get_sns_token'
// 钉钉获取token
export const GET_DINGTALK_USERINFO = 'https://oapi.dingtalk.com/sns/getuserinfo'
// 微信token
export const GET_WX_TOKEN = `${HOST}/je/app/TransferController/transferWeChat`
