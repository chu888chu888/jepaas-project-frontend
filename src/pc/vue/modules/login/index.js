import { install } from '../../install.js';
import Login from './page/login.vue';
// 安装组件
install('login', Login, () => {
  // 自动初始化组建
  const loginDom = document.createElement('div');
  loginDom.innerHTML = '<login></login>';
  document.body.appendChild(loginDom);
  new Vue({
    el: loginDom,
  });
});


// function JE-I18n(options){
//   let {messages} = options
//   let langs = Object.keys(messages)
//   langs.forEach(lang=>{
//     messages[lang] = strats(lang)
//   })

//   let strats = [
//     en: function(lang){
//       let pluginsLang = lang
//       let commonLang = commonLang
//       return  MergerLang(pluginsLang,commonLang)
//     }
//   ]
//   return new VueI18n(options);
// }