

const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const util = require('./util.js');

const resolve = dir => path.resolve(__dirname, '../../', dir || '');
const root = resolve(); // 根目录
const output = resolve('static/ux/je'); // 输出目录
const srcRoot = resolve('src/app'); // 源码目录
const jeSrcRoot = resolve('src-je'); // 源码目录
const includeDir = [jeSrcRoot, srcRoot];
const mode = process.env.NODE_ENV; // 环境
const dev = mode === 'development'; // 开发

let cfg = {};
if (dev) {
  cfg = require('./webpack.dev.js');
} else {
  cfg = require('./webpack.prod.js');
}
const entrys = util.buildJEEntry();
const webpackConfig = merge(
  {
    context: root,
    entry: entrys,
    devtool: 'none',
    externals: {
      // 不打包的第三方库
      vue: 'Vue',
    },
    stats: {},
    output: {
      path: output,
      filename: '[name].min.js',
      libraryTarget: 'umd',
      umdNamedDefine: true,
    },
    resolve: {
      extensions: ['.js', '.vue', '.json', '.css', '.less'],
      alias: {
        '@': srcRoot,
        '@root': root,
        node_modules: resolve('node_modules'),
      },
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          loader: 'babel-loader',
          include: includeDir,
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          include: includeDir,
        },
        {
          enforce: 'pre',
          test: /\.(vue|(j|t)sx?)$/,
          exclude: [
            /node_modules/,
          ],
          use: [
            {
              loader: 'eslint-loader',
              options: {
                extensions: [
                  '.js',
                  '.jsx',
                  '.vue',
                ],
                cache: true,
                cacheIdentifier: '4d9a30a4',
                emitWarning: true,
                emitError: false,
                formatter() { /* omitted long function */ },
              },
            },
          ],
        },
        {
          test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
          loader: 'url-loader',
          include: includeDir,
          options: {
            limit: -1,
            publicPath: util.buildPublicPath,
            name(url) {
              return util.buildAssetsUrl(url, 'images');
            },
          },
        },
        {
          test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
          loader: 'url-loader',
          include: includeDir,
          options: {
            limit: 10000,
            publicPath: util.buildPublicPath,
            name(url) {
              return util.buildAssetsUrl(url, 'media');
            },
          },
        },
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
          loader: 'url-loader',
          include: includeDir,
          options: {
            limit: 10000,
            publicPath: util.buildPublicPath,
            name(url) {
              return util.buildAssetsUrl(url, 'fonts');
            },
          },
        },
      ],
    },
    plugins: [
      new VueLoaderPlugin(),
      new CleanWebpackPlugin(['dist/'], {
        root: path.resolve(__dirname, '../../'),
        verbose: true,
        dry: false,
      }),
      new webpack.DefinePlugin({
        'process.env': {
          // 项目变量
          PRODUCT_CONFIG: JSON.stringify(process.env.PRODUCT_CONFIG),
        },
      }),
      new webpack.ContextReplacementPlugin(/moment[\\/]locale$/, /^\.\/(zh-cn)$/),
    ],
  },
  cfg
);
module.exports = webpackConfig;
